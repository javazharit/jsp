<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>***JSP MAGIC PAGE***</title>
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <script src="js/validation.js" type="text/javascript" language="JavaScript"></script>
</head>

<body>

<%
    Object param = session.getAttribute("field");
    String str = String.valueOf(param);
    if (param == null) {
        str = "1";
    }
%>

<div>
    <form method="post" action="handle" name="formhandle" onsubmit="return isEmpty()">
        <table border="0" cellspacing="5" cellpadding="5">
            <caption><h2 class="center">HTML Form</h2></caption>
            <tr>
                <td align="right" valign="top"><b>Magic field</b></td>
                <td>
                    <label>
                        <input type="text" size="50" name="field" value="<%=str%>"
                               onkeypress="return isNumberKey(event)">
                    </label>
                </td>
            </tr>
            <tr>
                <td align="right" colspan="2">
                    <input type="submit" value="Submit">
                </td>
            </tr>
        </table>
    </form>
</div>

<div id="footer">
    <p>&copy; 2015 Artur Babayan</p>
    <p>Contact info:<a href="mailto:arturarmbabayan@gmail.com">arturarmbabayan@gmail.com</a></p>
</div>

</body>
</html>
