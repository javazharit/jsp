package main.java.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

public class MainServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter pw = response.getWriter();

        String fieldString = request.getParameter("field");

        // validate user inserted numbers
        if (fieldString.matches("\\d+")) {
            int fieldInt = Integer.parseInt(fieldString);

            HttpSession session = request.getSession();
            session.setAttribute("field", ++fieldInt);

            request.getRequestDispatcher("index.jsp").forward(request, response);
        } else {
            pw.write("<h1>Please enter only a number!</h1>");

            request.getRequestDispatcher("index.jsp").include(request, response);
        }
    }
}
